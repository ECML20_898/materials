
#ifndef GDE_RELATIONSHIP_MAP_HH
#define GDE_RELATIONSHIP_MAP_HH

#include "episode.hh"

class RelationshipMap {

private:
    std::map<std::pair<Item, Item>, Episode::Relationship> constraints_;

public:
    RelationshipMap(const Episode& episode) {
        constraints_ = episode.get_constraint_map();
    }

    std::string get_relationship(const std::pair<Item, Item>& key) const {
        auto it = constraints_.find(key);
        if (it == constraints_.end())
            return "";
        return get_relationship_string(it->second);
    }

    std::string get_relationship_string(const Episode::Relationship& r) const {
        switch (r) {
            case Episode::Relationship::after :
                return "->";
            case Episode::Relationship::strictly_after :
                return "|->";
            case Episode::Relationship::same_time :
                return "--";
            case Episode::Relationship::before :
                return "<-";
            case Episode::Relationship::strictly_before :
                return "<-|";
            default:
                return "";
        }
    }
};


#endif //GDE_RELATIONSHIP_MAP_HH
