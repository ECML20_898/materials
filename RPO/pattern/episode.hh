#ifndef GDE_EPISODE_HH
#define GDE_EPISODE_HH

#include <iostream>
#include <sstream>

#include "item.hh"
#include "multiset.hh"
#include "occurrence_map.hh"
#include "tid_map.hh"

class Episode {

public:
    enum class Relationship {
        after, before, same_time, strictly_after, strictly_before
    };

private:
    Multiset multiset_;
    std::map<std::pair<Item, Item>, Relationship> constraints_;
    std::map<std::pair<Item, Item>, unsigned> map_couple_to_occurrence_index_;

    std::map<unsigned, unsigned> frequencies_;
    bool valid_{true};
    TidMap tid_list_;

public:
    // Only for SWIG
    // TODO: use proper ifdef
    Episode() = default;
    Episode(const Multiset& multiset, const std::vector<int>& tcs, const std::vector<std::pair<int*, int*>>& values);
    Episode(const Multiset& multiset, const std::vector<std::pair<int, int>>& constraints);

    const std::map<std::pair<Item, Item>, Relationship>& get_constraint_map() const;

    const std::map<unsigned, unsigned>& get_frequencies() const;
    unsigned get_frequency(unsigned class_id) const;
    const Multiset& get_multiset() const;
    const TidMap & get_tid_list() const;

    bool is_valid() const {
        return valid_;
    }

    void set_frequencies(const OccurrenceMap &occurrences);

    void set_frequencies(const std::map<unsigned, unsigned>& frequencies);
    void set_tid_list(const TidMap &tid_list);

    void set_not_valid() {
        valid_ = false;
    }

    explicit operator std::string() const;

private:
    unsigned compute_class_frequency(const std::vector<std::vector<std::vector<int>>> &class_occurrences) const;
    std::pair<unsigned, unsigned> get_item_couple(unsigned i, unsigned n) const;
    std::string get_string_constraints() const;
    bool occurs_in_bag(const std::vector<std::vector<int>> &bag) const;
    bool occurs_in_instance(const std::vector<int> &instance) const;
};


#endif //GDE_EPISODE_HH
