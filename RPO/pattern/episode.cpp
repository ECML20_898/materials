#include "episode.hh"

Episode::Episode(const Multiset &multiset, const std::vector<int> &tcs,
                 const std::vector<std::pair<int *, int *>> &values): multiset_(multiset) {
    std::vector<std::pair<unsigned, unsigned>> item_vector = multiset_.get_item_vector();
    auto n = static_cast<unsigned>(item_vector.size());

    for (unsigned x = 0; x < tcs.size(); x++) {
        std::pair<unsigned, unsigned> couple = get_item_couple(static_cast<unsigned>(tcs[x]), n);
        Item source(item_vector[couple.first].first, item_vector[couple.first].second);
        Item target(item_vector[couple.second].first, item_vector[couple.second].second);
        map_couple_to_occurrence_index_[{source, target}] = static_cast<unsigned>(tcs[x]);

        if (values[x].first == NULL || *values[x].first == -1) {
            if (values[x].second != NULL) {
                if (*values[x].second == -1)
                    constraints_[{source, target}] = Relationship::strictly_before;
                else if (*values[x].second == 0)
                    constraints_[{source, target}] = Relationship::before;
            }
        }
        else if (*values[x].first == 0) {
            if (*values[x].second == 0)
                constraints_[{source, target}] = Relationship::same_time;
            else
                constraints_[{source, target}] = Relationship::after;
        }
        else
            constraints_[{source, target}] = Relationship::strictly_after;
    }
}

Episode::Episode(const Multiset &multiset, const std::vector<std::pair<int, int>>& constraints): multiset_(multiset) {
    std::vector<std::pair<unsigned, unsigned>> item_vector = multiset_.get_item_vector();
    auto n = static_cast<unsigned>(item_vector.size());

    for (unsigned i = 0; i < constraints.size(); i++) {
        std::pair<unsigned, unsigned> couple = get_item_couple(i, n);
        Item source(item_vector[couple.first].first, item_vector[couple.first].second);
        Item target(item_vector[couple.second].first, item_vector[couple.second].second);
        map_couple_to_occurrence_index_[{source, target}] = i;

        if (constraints[i].first == -1) {
            if (constraints[i].second == -1)
                constraints_[{source, target}] = Relationship::strictly_before;
            else if (constraints[i].second == 0)
                constraints_[{source, target}] = Relationship::before;
        }
        else if (constraints[i].first == 0) {
            if (constraints[i].second == 0)
                constraints_[{source, target}] = Relationship::same_time;
            else
                constraints_[{source, target}] = Relationship::after;
        }
        else
            constraints_[{source, target}] = Relationship::strictly_after;
    }
}

unsigned Episode::compute_class_frequency(
        const std::vector<std::vector<std::vector<int>>> &class_occurrences) const {
    unsigned freq = 0;
    for (const auto& bag : class_occurrences)
        if (occurs_in_bag(bag))
            freq++;
    return freq;
}

const std::map<std::pair<Item, Item>, Episode::Relationship>& Episode::get_constraint_map() const {
    return constraints_;
}

const std::map<unsigned, unsigned>& Episode::get_frequencies() const {
    return frequencies_;
}

unsigned Episode::get_frequency(unsigned class_id) const {
    return frequencies_.find(class_id)->second;
}

const Multiset& Episode::get_multiset() const {
    return multiset_;
}

const TidMap & Episode::get_tid_list() const {
    return tid_list_;
}

std::pair<unsigned, unsigned> Episode::get_item_couple(unsigned i, unsigned n) const {
    unsigned source = 0;
    unsigned temp_i = i;

    while(temp_i >= n - source -1) {
        temp_i -= (n - source -1);
        source++;
    }

    return {source, source+temp_i+1};
}

std::string Episode::get_string_constraints() const {
    std::ostringstream str_constraints;
    for (std::pair<std::pair<Item, Item>, Relationship > constraint : constraints_) {
        std::pair<Item, Item> couple = constraint.first;
        str_constraints << multiset_.get_event_name(couple.first.get_item()) << ":" << couple.first.get_card();
        switch (constraint.second) {
            case Relationship::strictly_before:
                str_constraints << " <-| ";
                break;
            case Relationship::before:
                str_constraints << " <- ";
                break;
            case Relationship::same_time:
                str_constraints << " -- ";
                break;
            case Relationship::after:
                str_constraints << " -> ";
                break;
            case Relationship::strictly_after:
                str_constraints << " |-> ";
                break;
        }
        str_constraints << multiset_.get_event_name(couple.second.get_item()) << ":" << couple.second.get_card() << std::endl;
    }
    return str_constraints.str();
}

bool Episode::occurs_in_bag(const std::vector<std::vector<int>> &bag) const {
    for (const auto& instance : bag)
        if (occurs_in_instance(instance))
            return true;
    return false;
}

bool Episode::occurs_in_instance(const std::vector<int> &instance) const {
    for (const auto& constraint : constraints_) {
        unsigned index = map_couple_to_occurrence_index_.find(constraint.first)->second;
        switch (constraint.second) {
            case Relationship::strictly_before:
                if (instance[index] >= 0)
                    return false;
                break;
            case Relationship::before:
                if (instance[index] > 0)
                    return false;
                break;
            case Relationship::same_time:
                if (instance[index] != 0)
                    return false;
                break;
            case Relationship::after:
                if (instance[index] < 0)
                    return false;
                break;
            case Relationship::strictly_after:
                if (instance[index] <= 0)
                    return false;
                break;
        }
    }
    return true;
}

void Episode::set_frequencies(const OccurrenceMap &occurrences) {
    for (const auto &class_occurrences : occurrences)
            frequencies_[class_occurrences.first] = compute_class_frequency(class_occurrences.second);
}

void Episode::set_frequencies(const std::map<unsigned, unsigned>& frequencies) {
    frequencies_[0] = frequencies.find(0)->second;
    frequencies_[1] = frequencies.find(1)->second;
}

void Episode::set_tid_list(const TidMap &tid_list) {
    tid_list_[0] = tid_list[0];
    tid_list_[1] = tid_list[1];
}

Episode::operator std::string() const {
    std::ostringstream str;
    str << std::string(multiset_) << std::endl << get_string_constraints();
    if (frequencies_.find(0) != frequencies_.end() && frequencies_.find(1) != frequencies_.end())
        str << "Support: " << get_frequency(1) << "/" << get_frequency(0);
    return str.str();
}