import os
import pandas as pd
import numpy as np
import subprocess
import sys
import copy
import timeit
import signal
import psutil

class TimeoutException(Exception):
    def __str__(self):
        return "-1"
    
class MemoryoutException(Exception):
    def __str__(self):
        return "-2"
    
class hardware_limit:
    def reinit(self):
        self.running = True
        self.initial_time = -1
        
    def __init__(self, timeout, memory_percent):
        self.timeout = timeout
        self.memory_percent = memory_percent
    
    def __call__(self, f, *args):
        def handler(signum, frame):
            if self.running:
                if psutil.virtual_memory()._asdict()['percent'] > self.memory_percent:
                    signal.alarm(0)
                    self.running = False
                    raise MemoryoutException()
                signal.signal(signal.SIGALRM, handler)
                signal.alarm(1)
                if self.initial_time == -1:
                    self.initial_time = timeit.default_timer()
                elif self.timeout <= timeit.default_timer() - self.initial_time:
                    signal.alarm(0)
                    self.running = False
                    raise TimeoutException()

        def new_f(*args):
            self.reinit()
            signal.signal(signal.SIGALRM, handler)
            signal.alarm(1)
            res = f(*args) 
            signal.alarm(0)
            return res

        new_f.__name__ = f.__name__
        return new_f
                

sys.path.append("../RPO/SWIG/rpo")
import rpo

#########################
#                       #
#       Dataset         #
#                       #
#########################


class Dataset:
    def __init__(self, item_mapping=[]):
        self.item_mapping = item_mapping
        self.reverse_item_mapping = {}
        self.db = {}
        self.db_seq = {}
        self.max_occ = {}
        self.group = {}
        self.inv_group = {}

    def get_features(self, episodes, match_functor, train=None):
        X = []
        y = []

        for file_id in range(len(self.db.keys())):
            file = list(self.db.keys())[file_id]
            for sid in range(len(self.db_seq[file])):
                x = []
                for episode in episodes:
                    x.append(match_functor(self, file, sid, episode, train))
                X.append(x)
                y.append(file_id)

        return X, y

    def shuffle(self):
        for label in self.db:
            permut = np.random.permutation(len(self.db[label][0]))
            self.db[label] = [np.array(item)[permut.tolist()].tolist() for item in self.db[label]]
            if len(self.db_seq) > 0:
                self.db_seq[label] = np.array(self.db_seq[label])[permut.tolist()].tolist()

    def load_vertical_sequences(self, fi, label):
        if label not in self.db:
            self.db[label] = []
        with open(fi, 'r') as fin:
            nbseq = 0
            max_occ = 0
            db = self.db[label]
            for line in fin:
                lline = line.split()
                if len(lline) > 0 and len(lline) % 2 == 0:
                    for i in range(0, len(lline), 2):
                        item = lline[i]
                        time = int(lline[i + 1])

                        if not item in self.reverse_item_mapping:
                            self.reverse_item_mapping[item] = len(self.item_mapping)
                            self.item_mapping.append(item)
                        it = self.reverse_item_mapping[item]

                        for _ in range(len(db), len(self.item_mapping)):
                            db.append([])
                        for _ in range(len(db[it]), nbseq + 1):
                            db[it].append([])
                        db[it][nbseq].append(time)
                        if len(db[it][nbseq]) > max_occ:
                            max_occ = len(db[it][nbseq])
                    nbseq += 1
            for item in db:
                for _ in range(len(item), nbseq):
                    item.append([])
            self.max_occ[label] = max_occ

    def load_sequences(self, filename, label):
        if label not in self.db_seq:
            self.db_seq[label] = []
        with open(filename, 'r') as fin:
            for line in fin:
                l = line.replace("\n", "")
                sequence = l.split(" ")
                self.db_seq[label].append([(e, int(t))
                                           for e, t in zip(sequence[0::2], sequence[1::2])
                                           ])

    def occurrences(self, label, sid, multiset, return_first=False):
        occurrences = []
        for item in multiset:
            item_type = item[0]
            item_id = self.reverse_item_mapping[item_type]
            if item_id < len(self.db[label]) and len(self.db[label][item_id][sid]) >= item[1]:
                occurrences.append(self.db[label][item_id][sid])
                if return_first:
                    return occurrences
            else:
                return []
        return occurrences

    def write_sequences(self, filename, label, tid_list=None, mode='w', multiset=None):
        with open(filename, mode) as fout:
            if multiset:
                tlist = tid_list
                if not tid_list:
                    tlist = list(range(len(self.db_seq[label])))
                for tid in tlist:
                    seq = []
                    invalid_seq = False
                    for item, card in multiset:
                        item_id = self.reverse_item_mapping[item]
                        if item_id < len(self.db[label]) and len(self.db[label][item_id][tid]) >= card:
                            seq += [(item, t) for t in self.db[label][item_id][tid]]
                        else:
                            invalid_seq = True
                            break
                    if not invalid_seq:
                        fout.write(" ".join([str(e) + " " + str(t) for (e, t) in seq]) + "\n")
            else:
                tlist = tid_list
                if not tid_list:
                    tlist = list(range(len(self.db_seq[label])))
                for tid in tlist:
                    fout.write(" ".join([str(e) + " " + str(t) for (e, t) in self.db_seq[label][tid]]) + "\n")


###########################
#                         #
#        Functions        #
#                         #
###########################


def get_closed_episodes(dataset, label, fmin, gmin, relevant=False):
    opt = rpo.CPEngineOptions("rpo")
    opt.set_solutions(0)
    opt.closed_ = True
    opt.output_tid_lists_ = False
    opt.minimal_length_ = 0
    opt.no_output()
    opt.set_export_episodes()
    opt.minimal_growth_rate_ = gmin
    opt.relevant_ = relevant
    
    rpo.init_database(opt)

    pos = copy.deepcopy(dataset.db[label])
    opt.minimal_support_ = int(len(pos[0]) * fmin)
    for item in pos:
        support = 0
        for seq in item:
            if len(seq) > 0:
                support += 1
        if support < opt.minimal_support_:
            for i in range(len(item)):
                item[i] = []
    pos = rpo.ClassVerticalDatabase(pos)
    
    neg = [[x for other in dataset.db if not other == label
              and len(dataset.db[other]) > item
              for x in dataset.db[other][item]]
                for item in range(len(dataset.db[label]))]

    max_seq_num = max([len(item) for item in neg])
    for x in range(len(neg)):
        for k in range(len(neg[x]), max_seq_num):
            neg[x].append([])

    neg = rpo.ClassVerticalDatabase(neg)
    
    rpo.set_item_mapping(opt, rpo.StringVector(dataset.item_mapping))
    rpo.set_positives(opt, pos)
    rpo.set_negatives(opt, neg)

    
    
    print(label)
    rpo.run(opt)
    return opt.get_episodes()
    
# @hardware_limit(3600, 80)
def run_xp(fmin, gmin, dataset, label, relevant):
    res = []
    substart = timeit.default_timer()
    nb_episodes = len(get_closed_episodes(dataset, label, fmin, gmin, relevant=relevant))
    return [label, fmin, gmin, nb_episodes, timeit.default_timer() - substart]
    
def run(dataset, previous_res, forbidden_label, fmin, gmin, relevant):
    res = []
    start = timeit.default_timer()
    total = 0
    ex_type = "closed"
    if relevant:
        ex_type = "relevant"
    for label in dataset.db:
        if label not in forbidden_label:
            try:
                res.append(run_xp(fmin, gmin, dataset, label, relevant))
                total += res[-1][-2]
            except (TimeoutException, MemoryoutException) as e:
                forbidden_label.append(label)
                res.append([label, fmin, gmin, 0, str(e)])
    res.append(['all_episodes', fmin, gmin, total, timeit.default_timer() - start])
    return res  
    
def output(res_file, results):
    with open(res_file, 'a') as fout:
        fout.write(",".join(map(str,results)) + "\n")
        
def previous_forbidden_label(row):
    key = row["dataset"]
    if key not in default_forbidden_label:
        default_forbidden_label[key] = []
    default_forbidden_label[key].append(row["label"])
    
    
####################
#                  #
#       Main       #
#                  #
####################


res_file = "asl_bu.csv"
if not os.path.isfile(res_file):
    output(res_file, ["dataset", "extraction_type", "label", "fmin", "gmin", "pattern_number", "time"])
            
datasets = ["blocks", "asl-bu"]
default_forbidden_label = {"blocks": ["assemble"]}

previous_res = pd.read_csv(res_file)
previous_fail = previous_res[(previous_res.time<0)]
previous_fail.apply(previous_forbidden_label, axis=1)

datasets_dir = "../datasets"

for gmin in [0,1,2]:   
    for data in datasets: 
        forbidden_label = []
        if data in default_forbidden_label:
            forbidden_label = default_forbidden_label[data]
        print(forbidden_label)
        dataset_dir = datasets_dir + "/" + data
        dataset = Dataset()
        for fi in os.listdir(dataset_dir):
            if fi.endswith(".dat"):
                dataset.load_vertical_sequences(dataset_dir + "/" + fi, fi.split(".dat")[0])
                dataset.load_sequences(dataset_dir + "/" + fi, fi.split(".dat")[0])

        for x in range(6,1,-1): 
            fmin = x/10.0
            results = run(dataset, previous_res, forbidden_label, fmin, gmin, True)
            for result in results:
                output(res_file, [data, "relevant"] + result)

            results = run(dataset, previous_res, forbidden_label, fmin, gmin, False)
            for result in results:
                output(res_file, [data, "closed"] + result)


